/**
 * AngularJS
 * @author Wahyu Sanjaya <wahyu.sanjaya@emeriocorp.com>
 */

var mainApp = angular.module('mainApp', [
  'ngRoute'
]);

mainApp.config(function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "templates/page1.html", controller: ""})
    // Pages
    .when("/page2", {templateUrl: "templates/page2.html", controller: ""})
    // else 404
    .otherwise("/404", {templateUrl: "templates/404.html", controller: ""});
});

mainApp.factory('MathService', function() {
  var factory = {};
  factory.plus = function(a, b) {
    return a+b
  }
  factory.minus = function(a, b) {
    return a-b
  }
  factory.kali = function(a, b) {
    return a*b
  }
  factory.bagi = function(a, b) {
    return a/b
  }
  return factory;
});

mainApp.service('CalcService', function(MathService) {
  this.square1 = function(a,b) {
    return MathService.kali(a,b);
  }
  this.square2 = function(a,b) {
    return MathService.bagi(a,b);
  }
  this.square3 = function(a,b) {
    return MathService.plus(a,b);
  }
  this.square4 = function(a,b) {
    return MathService.minus(a,b);
  }
});

mainApp.controller('CalcController', function($scope, CalcService) {
  $scope.square1 = function() {
    $scope.result = CalcService.square1($scope.number,$scope.number2);
  }
  $scope.square2 = function() {
    $scope.result = CalcService.square2($scope.number,$scope.number2);
  }
  $scope.square3 = function() {
    $scope.result = CalcService.square3($scope.number,$scope.number2);
  }
  $scope.square4 = function() {
    $scope.result = CalcService.square4($scope.number,$scope.number2);
  }
});
